export const state = () => ({
    id: 0,
    company: 'tester',
    qwilr: 'http://www.qwilr.com',
    authenticated: false,
    // billingFirstName: 'first_name',
    // billingLastName: 'last_name',
  })
  
  export const mutations = {
    add(state, wpUser) {
      console.log('wpUser passed in:'); //successful
      console.dir(wpUser);
      //console.log('first name: ' + wpUser.user_meta);
      //console.log('wpUser?: ' + typeof(wpUser));
      //console.log(text); successful
      state.id = wpUser.id;
      
      state.qwilr = wpUser.qwilr;
      //console.log('qwilr in state: ' + state.qwilr);
      
      if(wpUser.user_meta){
        console.log('meta store add');
        state.billingCompany = wpUser.user_meta.billing_company[0];

        state.billingFirstName = wpUser.user_meta.billing_first_name[0];
        state.billingLastName = wpUser.user_meta.billing_last_name[0];
        
        state.billingEmail = wpUser.user_meta.billing_email[0];
        state.billingPhone = wpUser.user_meta.billing_phone[0];

        state.billingAddress1 = wpUser.user_meta.billing_address_1[0];
        state.billingAddress2 = wpUser.user_meta.billing_address_2[0];
        
        state.billingCity = wpUser.user_meta.billing_city[0];
        state.billingState = wpUser.user_meta.billing_state[0];
        state.billingCountry = wpUser.user_meta.billing_country[0];
        state.billingZip = wpUser.user_meta.billing_postcode[0];

        state.shippingCompany = wpUser.user_meta.shipping_company[0];

        state.shippingFirstName = wpUser.user_meta.shipping_first_name[0];
        state.shippingLastName = wpUser.user_meta.shipping_last_name[0];

        state.shippingEmail = wpUser.user_meta.shipping_email[0];
        state.shippingPhone = wpUser.user_meta.shipping_phone[0];

        state.shippingAddress1 = wpUser.user_meta.shipping_address_1[0];
        state.shippingAddress2 = wpUser.user_meta.shipping_address_2[0];
        
        state.shippingCity = wpUser.user_meta.shipping_city[0];
        state.shippingState = wpUser.user_meta.shipping_state[0];
        state.shippingCountry = wpUser.user_meta.shipping_country[0];
        state.shippingZip = wpUser.user_meta.shipping_postcode[0];

        //state.qwilr = wpUser.qwilr[0];
      }

      // if(wpUser.userinfo){
      //   state.billingFirstName = wpUser.userinfo.first_name[0];
      //   state.billingLastName = wpUser.userinfo.last_name;
        
      //   state.billingEmail = wpUser.userinfo.billing_email;
      //   state.billingPhone = wpUser.userinfo.billing_phone;

      //   state.billingAddress1 = wpUser.userinfo.billing_address_1;
      //   state.billingAddress2 = wpUser.userinfo.billing_address_2;
        
      //   state.billingCountry = wpUser.userinfo.billing_country;
      //   state.billingCity = wpUser.userinfo.billing_city;
      //   state.billingState = wpUser.userinfo.billing_state;
      //   state.billingZip = wpUser.userinfo.billing_postcode;

      //   state.shippingAddress1 = wpUser.userinfo.shipping_address_1;
      //   state.shippingAddress2 = wpUser.userinfo.shipping_address_2;
        
      //   state.shippingCountry = wpUser.userinfo.shipping_country;
      //   state.shippingCity = wpUser.userinfo.shipping_city;
      //   state.shippingState = wpUser.userinfo.shipping_state;
      //   state.shippingZip = wpUser.userinfo.shipping_postcode;
      // }
      else{
        console.log('wpData store add');
        state.billingCompany = wpUser.billing_company;
        
        state.billingFirstName = wpUser.billingFirstName;
        state.billingLastName = wpUser.billingLastName;
        
        state.billingEmail = wpUser.billingEmail;
        state.billingPhone = wpUser.billingPhone;

        state.billingAddress1 = wpUser.billingAddress1;
        state.billingAddress2 = wpUser.billingAddress2;
        
        state.billingCountry = wpUser.billingCountry;
        state.billingCity = wpUser.billingCity;
        state.billingState = wpUser.billingState;
        state.billingZip = wpUser.billingZip;


        state.shippingCompany = wpUser.shipping_company;

        state.shippingFirstName = wpUser.shippingFirstName;
        state.shippingLastName = wpUser.shippingLastName;

        state.shippingEmail = wpUser.shippingEmail;
        state.shippingPhone = wpUser.shippingPhone;

        state.shippingAddress1 = wpUser.shippingAddress1;
        state.shippingAddress2 = wpUser.shippingAddress2;
        
        state.shippingCountry = wpUser.shippingCountry;
        state.shippingCity = wpUser.shippingCity;
        state.shippingState = wpUser.shippingState;
        state.shippingZip = wpUser.shippingZip;

        state.qwilr = wpUser.qwilr;
      }
      //console.log('user added done');
      //console.log(text.id); // prints 51!
    },

    update(state, item){
      console.log('item: ' + item.id + ', value: ' + item.value);
      // var temp = item.id;
      // console.log('value: ' + item.value);
      // console.log('updated?: ' + state.temp);
      
      //state.billingFirstName = item.value;
      state[item.id] = item.value;
      //console.log(state);
    }
  //   remove(state, { todo }) {
  //     state.list.splice(state.list.indexOf(todo), 1)
  //   },
    
  // company(state, user) {
    //   user.company = !user.company
    // },
    // updateBillingFirstName(state, value) {
    //   state.billingFirstName = value;
    //   console.log('action updated first_name to: ' + value);
    // },
    // updateBillingLastName(state, value) {
    //   state.billingLastName = value;
    //   console.log('action updated last_name to: ' + value);
    // },
    // updateCompany(state, value){
    //   state.company = value;
    // },
    // updateId(state, value){
    //   state.id = value;
    // },
    // updateBillingEmail(state, value) {
    //   state.billingEmail = value;
    //   console.log('action updated email to: ' + value);
    // },
    // updateBillingPhone(state, value) {
    //   state.billingPhone = value;
    //   console.log('action updated phone to: ' + value);
    // },
    // updateBillingAddress1(state, value) {
    //   state.billingAddress1 = value;
    //   console.log('action updated billing address 1 to: ' + value);
    // },
    // updateBillingAddress2(state, value) {
    //   state.billingAddress2 = value;
    //   console.log('updated billing address 2 to: ' + value);
    // },

    // updateBillingCountry(state, value) {
    //   state.billingCountry = value;
    //   console.log('updated country to: ' + value);
    // },
    // updateBillingCity(state, value) {
    //   state.billingCity = value;
    //   console.log('updated city to: ' + value);
    // },
    // updateBillingState(state, value) {
    //   state.billingState = value;
    //   console.log('updated state to: ' + value);
    // },
    // updateBillingZip(state, value) {
    //   state.billingZip = value;
    //   console.log('updated zip to: ' + value);
    // },

    // updateShippingAddress1(state, value) {
    //   state.shippingAddress1 = value;
    //   console.log('updated shipping address 1 to: ' + value);
    // },
    // updateShippingAddress2(state, value) {
    //   state.shippingAddress2 = value;
    //   console.log('updated shipping address 2 to: ' + value);
    // },

    // updateShippingCountry(state, value) {
    //   state.shippingCountry = value;
    //   console.log('updated shipping country to: ' + value);
    // },
    // updateShippingCity(state, value) {
    //   state.shippingCity = value;
    //   console.log('updated shipping city to: ' + value);
    // },
    // updateShippingState(state, value) {
    //   state.shippingState = value;
    //   console.log('updated shipping state to: ' + value);
    // },
    // updateShippingZip(state, value) {
    //   state.shippingZip = value;
    //   console.log('updated shipping zip to: ' + value);
    // },
  }