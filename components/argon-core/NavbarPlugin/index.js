import Navbar from './NavBar.vue';
import NavbarItem from './NavbarItem.vue';

const NavbarStore = {
  showNavbar: false,
  navbarLinks: [],
  isMinimized: false,
  breakpoint: 1200,
  hovered: false,
  displayNavbar(value) {
    if (window.innerWidth > this.breakpoint) {
      return;
    }
    this.showNavbar = value;
    let docClasses = document.body.classList
    if (value) {
      docClasses.add('g-sidenav-pinned')
      docClasses.add('g-sidenav-show')
      docClasses.remove('g-sidenav-hidden')
    } else {
      docClasses.add('g-sidenav-hidden')
      docClasses.remove('g-sidenav-pinned')
      docClasses.remove('g-sidenav-show')
    }
  },
  toggleMinimize() {
    this.isMinimized = !this.isMinimized;
    let docClasses = document.body.classList
    if (this.isMinimized) {
      docClasses.add('g-sidenav-hidden')
      docClasses.remove('g-sidenav-pinned')
      docClasses.remove('g-sidenav-show')
    } else {
      docClasses.add('g-sidenav-pinned')
      docClasses.add('g-sidenav-show')
      docClasses.remove('g-sidenav-hidden')
    }
    if (this.hovered) {
      docClasses.add('g-sidenav-show')
    }
  },
  onMouseEnter() {
    this.hovered = true
    if (this.isMinimized) {
      document.body.classList.add('g-sidenav-show')
      document.body.classList.remove('g-sidenav-hidden')
    }
  },
  onMouseLeave() {
    this.hovered = false
    if (this.isMinimized) {
      let docClasses = document.body.classList
      docClasses.remove('g-sidenav-show')
      docClasses.add('g-sidenav-hide')
      setTimeout(() => {
        docClasses.remove('g-sidenav-hide')
        docClasses.add('g-sidenav-hidden')
      }, 300)
    }
  }
};

const NavbarPlugin = {
  install(Vue, options) {
    if (options && options.navbarLinks) {
      NavbarStore.navbarLinks = options.navbarLinks;
    }
    let app = new Vue({
      data: {
        navbarStore: NavbarStore
      }
    });
    Vue.prototype.$navbar = app.navbarStore;
    Vue.component('nav-bar', Navbar);
    Vue.component('navbar-item', NavbarItem);
  }
};

export default NavbarPlugin;
