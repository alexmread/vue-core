export default [
  {
    name: 'Monthly SEO Subscription',
    price: '$800'
  },
  {
    name: 'Subtotal',
    price: '$1700'
  },
  {
    name: 'Discounts',
    price: '$400'
  },
  {
    name: 'Total',
    price: '$1300'
  }
]