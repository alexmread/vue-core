export default [
  {
    name: 'Robert',
    email: 'robert@winwithmidas.com',
    role: 'view only',
  },
  {
    name: 'Alex',
    email: 'alex@winwithmidas.com',
    role: 'admin',
  },
  {
    name: 'Kris',
    email: 'kris@winwithmidas.com',
    role: 'admin',
  },
  {
    name: 'Cyr',
    email: 'cyr@winwithmidas.com',
    role: 'billing',
  }
]