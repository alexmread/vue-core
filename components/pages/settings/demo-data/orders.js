export default [
  {
    id: '8932',
    date: 'September 3, 2019',
    total: '$11157',
    status: 'Pending Payment',
    statusType: 'danger',
  },
  {
    id: '8930',
    date: 'September 12, 2019',
    total: '$5157',
    status: 'Complete',
    statusType: 'success',
  },
  {
    id: '9010',
    date: 'October 1, 2019',
    total: '$1057',
    status: 'Complete',
    statusType: 'success',
  },
  {
    id: '9391',
    date: 'October 3, 2019',
    total: '$2157',
    status: 'Complete',
    statusType: 'success',
  }
]