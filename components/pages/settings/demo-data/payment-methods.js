export default [
  {
    cardType: 'VISA',
    cardDigits: '5555',
    expirationMonth: 'APR',
    expirationYear: '2022',
    status: 'default',
    statusType: 'success',
    defaultCard: true
  },
  {
    cardType: 'AMEX',
    cardDigits: '1234',
    expirationMonth: 'MAY',
    expirationYear: '2023',
    status: 'valid',
    statusType: 'warning',
    defaultCard: false
  },
  {
    cardType: 'VISA',
    cardDigits: '5555',
    expirationMonth: 'AUG',
    expirationYear: '2019',
    status: 'expired',
    statusType: 'danger',
    defaultCard: false
  },
  {
    cardType: 'MASTERCARD',
    cardDigits: '9876',
    expirationMonth: 'JAN',
    expirationYear: '2023',
    status: 'valid',
    statusType: 'warning',
    defaultCard: false
  },
]