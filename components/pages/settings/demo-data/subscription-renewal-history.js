export default [
  {
    id: '111',
    date: 'January 1, 2020',
    status: 'Processing',
    total: '$400'
  },
  {
    id: '112',
    date: 'February 1, 2020',
    status: 'Processing',
    total: '$400'
  },
  {
    id: '113',
    date: 'March 1, 2020',
    status: 'Processing',
    total: '$400'
  },
  {
    id: '114',
    date: 'April 1, 2020',
    status: 'Processing',
    total: '$400'
  },
  {
    id: '115',
    date: 'May 1, 2020',
    status: 'Processing',
    total: '$400'
  },
]