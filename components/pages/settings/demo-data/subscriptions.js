export default [
  {
    name: 'Monthly SEO',
    nextPaymentDate: 'October 29, 2019',
    status: 'active',
    statusType: 'success',
    total: '450'
  },
  {
    name: 'Web Hosting',
    nextPaymentDate: 'November 2, 2019',
    status: 'active',
    statusType: 'success',
    total: '175'
  },
  {
    name: 'Facebook Ads',
    nextPaymentDate: 'November 3, 2019',
    status: 'active',
    statusType: 'success',
    total: '800'
  }
]