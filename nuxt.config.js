
/*!

=========================================================
* Nuxt Argon Dashboard PRO - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nuxt-argon-dashboard-pro
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

const pkg = require('./package')
console.log('ENV', process.env.NODE_ENV)

export default {
  target: 'static',
}

module.exports = {
  
  router: {
    base: '/',
    linkExactActiveClass: 'active',
    /* works great, but opted for Component beforeRouteEnter lifecycle instead */
    // extendRoutes(routes, resolve) {
      
    //     routes.forEach((item, index, object) => {
    //       console.log("curr route=", item.name, "==", item, ":", index);
    //       if(item.name == 'index'){
    //         object.splice(index, 1)
    //       }
    //     })

    //     routes.push(
    //       {
    //         name: 'index',
    //         path: '/',
    //         component: resolve(__dirname, 'pages/dashboard.vue'),
    //         chunkName: 'pages/dashboard',
    //         //alias: '/'
    //         redirect: '/dashboard'
    //       }
    //     )
    // }
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/' : 'https://mdashdev.local/',
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg' },
      // { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'},
      // { rel: 'stylesheet', href: '/wp-content/themes/midas-dashboard/assets/fonts/nucleo/css/nucleo.css'},
      // { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css', integrity: "sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/", crossorigin: "anonymous"}
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    // 'assets/css/nucleo/css/nucleo.css',
    'assets/sass/argon.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/dashboard/dashboard-plugin',
    {src: '~/plugins/dashboard/full-calendar', ssr: false },
    {src: '~/plugins/dashboard/world-map', ssr: false },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: [
      'vee-validate/dist/rules'
    ],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    },
    //extractCSS: false,
    extractCSS: process.env.NODE_ENV === 'production',
    //allChunks: false,
    babel: {
      plugins: [
        [
          "component",
          {
            "libraryName": "element-ui",
            "styleLibraryName": "theme-chalk"
          }
        ]
      ]
    },
  },

  generate: {
    dir: '../vue-dist'
  },

}
